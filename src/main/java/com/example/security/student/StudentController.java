package com.example.security.student;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("api/v1/students")
public class StudentController {

   private static final List<Student> STUDENTS = Arrays.asList(
           new Student(1,"joe"),
           new Student(2,"alex"),
           new Student(3,"bran")
   );

    @GetMapping("/{studentId}")
   public Student getStudent(@PathVariable("studentId") Integer studentId){
       return STUDENTS.stream()
                .filter(student -> student.getStudentId().equals(studentId))
                .findFirst().orElseThrow(() -> new IllegalArgumentException("Student"+ studentId + "does not exists"));

   }
}
